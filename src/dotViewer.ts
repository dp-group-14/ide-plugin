import * as vscode from 'vscode';
import { TextDocumentIdentifier } from 'vscode-languageclient/node';
import { client, clientReady, disposalQueue, extensionUri, resultHandlers } from './extension';
import * as fs from "fs";
import { ParameterDefinitions, getNonce, ResultNotification, ParameterDefinition } from './utils';


export namespace DotViewer {

    export let viewDot = vscode.commands.registerCommand('modest.viewDot', async () => {
        if (vscode.window.activeTextEditor === undefined ||
            vscode.window.activeTextEditor.document.uri === undefined) {
            return;
        }
        DotView.addOrOpen(vscode.window.activeTextEditor?.document.uri);
    });

    export let viewDotSameWindow = vscode.commands.registerCommand('modest.viewDotSameWindow', async () => {
        if (vscode.window.activeTextEditor === undefined ||
            vscode.window.activeTextEditor.document.uri === undefined) {
            return;
        }
        DotView.addOrOpen(vscode.window.activeTextEditor?.document.uri, true);
    });

    export let resetDotPosition = vscode.commands.registerCommand('modest.resetDotPosition', async () => {
        DotView.currentDotView?.postMessage({
            type: 'reset'
        });
    });

    export let dotZoomIn = vscode.commands.registerCommand('modest.dotZoomIn', async () => {
        DotView.currentDotView?.postMessage({
            type: 'zoomIn'
        });
    });

    export let dotZoomOut = vscode.commands.registerCommand('modest.dotZoomOut', async () => {
        DotView.currentDotView?.postMessage({
            type: 'zoomOut'
        });
    });

    export let dotSaveSvg = vscode.commands.registerCommand('modest.dotSaveSvg', async () => {
        DotView.currentDotView?.saveSvg();
    });

    export let dotCopySvg = vscode.commands.registerCommand('modest.dotCopySvg', async () => {
        DotView.currentDotView?.copySvg();
    });

    export let dotSaveDot = vscode.commands.registerCommand('modest.dotSaveDot', async () => {
        DotView.currentDotView?.saveDot();
    });

    export let dotCopyDot = vscode.commands.registerCommand('modest.dotCopyDot', async () => {
        DotView.currentDotView?.copyDot();
    });

    /**
     * A class to keep track of all information related to a dotView. 
     * For example: the current opened dotview, the parameters that are set for this dotview, etc.
     */
    class DotView {
        public static currentDotView: DotView | undefined;

        public static liveDotViews = new Map<vscode.Uri, DotView>();
        private static availableParameters: Array<ParameterDefinition> | undefined;
        private static filteredParameters: Array<string> = [
            'InvokeDot',
            'DotFormat',
            'DotOutputFile',
            'DeleteOutput',
            'OpenOutput',
            'PropertiesToConsider',
        ];

        private fileUri: vscode.Uri;
        private panel: vscode.WebviewPanel;
        private lastDotString: string | undefined;
        private lastSvgString: string | undefined;
        private dotParameters: Map<string, string> = new Map();
        private lastWorkingParameters: Map<string, string> = new Map();
        private lastRunToken: string | undefined;
        private optionsOpen: boolean = false;

        /**
         * Creates and opens a new dot view.
         * @param fileUri The Uri of the file the dot view is for
         * @param sameColumn true if the view should be opened in the same column as the current window
         */
        public constructor(fileUri: vscode.Uri, sameColumn = false) {
            this.fileUri = fileUri;

            this.panel = vscode.window.createWebviewPanel(
                // Webview id
                `liveDotView ${fileUri.path}`,
                // Webview title
                `[Preview] ${fileUri.path.split("/").pop()}`,
                // This will open the second column for preview inside editor
                sameColumn ? -1 : -2,
                {
                    // Enable scripts in the webview
                    enableScripts: true,
                    // And restrict the webview to only loading content from our extension's base directory.
                    localResourceRoots: [extensionUri]
                }
            );
            this.panel.webview.html = this.getHtml();

            // handle the disposal of the view
            this.panel.onDidDispose(() => {
                vscode.commands.executeCommand('setContext', 'modest:dotViewFocused', false);
                DotView.liveDotViews.delete(this.fileUri);
                if (this.lastRunToken) {
                    client?.sendRequest("modest/cancelRun", { runToken: this.lastRunToken });
                }
            });

            // If the viewState is changed (happens when you switch windows) send the info to the webview again,
            // since the webview's information could be deleted
            this.panel.onDidChangeViewState(
                e => {
                    vscode.commands.executeCommand('setContext', 'modest:dotViewFocused', e.webviewPanel.active);
                    if (e.webviewPanel.active) {
                        DotView.currentDotView = this;
                    }
                    // send the svg (if it exists)
                    if (this.lastSvgString) {
                        this.postMessage({
                            type: 'svg',
                            svg: this.lastSvgString,
                        });
                    }
                    // send the parameters (if they exist)
                    if (DotView.availableParameters) {
                        this.postMessage({
                            type: 'params',
                            params: DotView.availableParameters,
                        });
                    }
                    // send the parameters' values (if they exist)
                    if (this.dotParameters) {
                        this.postMessage({
                            type: 'paramValues',
                            values: Array.from(this.dotParameters.entries()),
                        });
                    }
                    // send if the options window should be open
                    this.postMessage({
                        type: 'optionsOpen',
                        optionsOpen: this.optionsOpen,
                    });
                },
            );

            //Handle received messages from the webview
            this.panel.webview.onDidReceiveMessage(
                message => {
                    switch (message.type) {
                        case 'paramUpdate':
                            // Update a parameter and update the dot
                            this.dotParameters.set(message.id, message.value);
                            this.updateDot();
                            break;
                        case 'paramReset':
                            // Reset the parameters and update the dot
                            this.dotParameters = new Map();
                            this.updateDot();
                            break;
                        case 'toggleOptions':
                            // Toggle the options
                            this.optionsOpen = !this.optionsOpen;
                            break;
                    }
                },
            );


            // update the dot if the focument was saved
            vscode.workspace.onDidSaveTextDocument(td => {
                if (td.uri === this.fileUri) {
                    this.updateDot();
                }
            });

            // Generate the dot for the first time
            this.updateDot();


            vscode.commands.executeCommand('setContext', 'modest:dotViewFocused', this.panel.active);
            DotView.currentDotView = this;
            DotView.liveDotViews.set(fileUri, this);

            // Get the parameters if they're not available yet and send them to the webview.
            if (!DotView.availableParameters) {
                client?.sendRequest<ParameterDefinitions>("modest/getParameters", { "toolName": "mosta (export-to-dot)" }).then(data => {
                    DotView.availableParameters = data.parameterDefinitions.filter(param => !DotView.filteredParameters.includes(param.id));
                    this.postMessage({
                        type: 'params',
                        params: DotView.availableParameters,
                    });
                });
            } else {
                this.postMessage({
                    type: 'params',
                    params: DotView.availableParameters,
                });
            }
        }

        /**
         * Create a new dotview or focus the dotivew with the given fileUri if it exists
         * @param uri the uri of the file the dotview is for
         * @param sameColumn true if the view should be opened in the same column as the current window
         */
        public static addOrOpen(uri: vscode.Uri, sameColumn = false) {
            if (!clientReady) {
                vscode.window.showErrorMessage("Server not ready yet, try again later");
                return;
            }
            var dotView = DotView.liveDotViews.get(uri);
            if (dotView !== undefined) {
                dotView.panel.reveal();
            } else {
                new DotView(uri, sameColumn);
            }
        }

        /**
         * Post a message to this dotView's webview
         * @param obj message to be posted
         */
        public postMessage(obj: any) {
            this.panel.webview.postMessage(obj);
        }

        /**
         * Save this dotView's svg
         */
        public saveSvg() {
            if (this.lastSvgString) {
                const defaultUri = vscode.workspace.workspaceFolders
                    ? vscode.Uri.joinPath(vscode.workspace.workspaceFolders[0].uri, `/analysis/${this.fileUri.path.split("/").pop()?.slice(0, -1 * '.modest'.length)}.svg`)
                    : undefined;
                vscode.window.showSaveDialog({ defaultUri: defaultUri }).then(f => {
                    if (f) {
                        fs.writeFileSync(f.fsPath, this.lastSvgString);
                    }
                });
            } else {
                vscode.window.showErrorMessage("There are no analysis results to be exported.");
            }
        }

        /**
         * Copy this dotView's svg
         */
        public copySvg() {
            if (this.lastSvgString) {
                vscode.env.clipboard.writeText(this.lastSvgString);
            } else {
                vscode.window.showErrorMessage("There are no analysis results to be exported.");
            }
        }

        /**
         * Save this dotView's dot
         */
        public saveDot() {
            if (this.lastDotString) {
                const defaultUri = vscode.workspace.workspaceFolders
                    ? vscode.Uri.parse(`{vscode.workspace.workspaceFolders[0].uri.fsPath}/analysis/${this.fileUri.path.split("/").pop()?.slice(0, -1 * '.modest'.length)}.dot`)
                    : undefined;
                vscode.window.showSaveDialog({ defaultUri: defaultUri }).then(f => {
                    if (f) {
                        fs.writeFileSync(f.fsPath, this.lastDotString);
                    }
                });
            } else {
                vscode.window.showErrorMessage("There are no analysis results to be exported.");
            }
        }

        /**
         * Copy this dotView's dot
         */
        public copyDot() {
            if (this.lastDotString) {
                vscode.env.clipboard.writeText(this.lastDotString);
            } else {
                vscode.window.showErrorMessage("There are no analysis results to be exported.");
            }
        }


        /**
         * Use the languageServer's modest/runTool method to convert the current file into a dot and update the webview accordingly
         */
        private updateDot() {
            let uri = this.fileUri.toString();

            // Collect all changed parameters
            let parameters = [];
            for (const entry of this.dotParameters.entries()) {
                parameters.push({
                    id: entry[0],
                    value: entry[1]
                });
            }
            let dotParametersCopy = new Map(this.dotParameters);

            let runToken = uri + JSON.stringify(this.dotParameters) + Date.now();

            let jsonObject = {
                textDocument: TextDocumentIdentifier.create(uri),
                toolName: "mosta (export-to-dot)",
                constants: [],
                parameters: parameters,
                runToken: "dot" + runToken
            };
            //Create a progress indicator that goed away when resolveProgress is called
            vscode.window.withProgress({ location: vscode.ProgressLocation.Window, title: `Loading new dot for ${this.fileUri.path.split("/").pop()}` }, async (progress, token) => {
                await new Promise<null>(async (resolveProgress, _) => {
                    try {
                        let resultHandler = (data: ResultNotification) => {
                            if (data.runToken === jsonObject.runToken) {
                                if (data.data && data.data !== "") {
                                    // Looks like that went successfully, we received a dot
                                    try {
                                        // Since this worked, let's save these parameters and the dot
                                        this.lastWorkingParameters = dotParametersCopy;
                                        this.lastDotString = data.data;
                                        // Generate and display an svg from this dot
                                        this.dotToSvg(runToken, resolveProgress);
                                    } catch (error) {
                                        console.error(error);
                                    }
                                } else {
                                    // Hmmmm, apparently this combination of parameters gave an error
                                    // Reset the parameters to the last known working parameterset
                                    this.dotParameters = new Map(this.lastWorkingParameters);
                                    this.postMessage({
                                        type: 'paramValues',
                                        values: Array.from(this.dotParameters.entries())
                                    });
                                }
                                // Remove this resulthandler from the list
                                resultHandlers.splice(resultHandlers.indexOf(resultHandler), 1);
                                resolveProgress(null);
                            }
                        };

                        resultHandlers.push(resultHandler);
                        disposalQueue.push({
                            dispose: function () {
                                resolveProgress(null);
                            }
                        });
                        await client?.sendRequest<string>("modest/runTool", jsonObject, token);
                    } catch (e) {
                        vscode.window.showErrorMessage("Internal error: " + e);
                        resolveProgress(null);
                        return;
                    }
                });
            });
        }

        /**
         * Convert this object's lastDotString to an svg using the language server's modest/dot method
         * @param runToken the runtoken to be used
         * @param resolveProgress a function which resolves the progress indicator when it's called
         */
        private dotToSvg(runToken: string, resolveProgress: (value: any | PromiseLike<any>) => void) {
            let jsonObject = {
                dot: this.lastDotString,
                runToken: "svg" + runToken
            };
            // Cancel the last run if it exists
            if (this.lastRunToken) {
                client?.sendRequest("modest/cancelRun", { runToken: this.lastRunToken });
            }
            // Set the last run to this
            this.lastRunToken = runToken;


            let resultHandler = (data: ResultNotification) => {
                if (data.runToken === jsonObject.runToken) {
                    if (data.data && data.data !== "") {
                        // The svg is received, set it in lastSvgString and pass it on to the webview
                        try {
                            this.lastSvgString = data.data;
                            this.postMessage({
                                type: 'svg',
                                svg: this.lastSvgString
                            });

                        } catch (error) {
                            console.error(error);
                        }
                    }
                    // Remove this resulthandler from the list
                    resultHandlers.splice(resultHandlers.indexOf(resultHandler), 1);
                    resolveProgress(null);

                }
            };

            resultHandlers.push(resultHandler);
            disposalQueue.push({
                dispose: function () {
                    resolveProgress(null);
                }
            });
            client?.sendRequest<string>("modest/dot", jsonObject);
        }



        /**
         * Generate the html for this dotView's webview
         * @returns the html for the webview
         */
        private getHtml() {
            let webview = this.panel.webview;
            const scriptUri = webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, "media", "dotview.js"));

            const styleResetUri = webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'media', 'reset.css'));
            const styleVSCodeUri = webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'media', 'vscode.css'));
            const styleMainUri = webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'media', 'dotview.css'));
            const styleCodicons = webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'node_modules', 'vscode-codicons', 'dist', 'codicon.css'));
            const fontCodicons = webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'node_modules', 'vscode-codicons', 'dist', 'codicon.ttf'));

            // Use a nonce to only allow a specific script to be run.
            const nonce = getNonce();

            return `
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="Content-Security-Policy" content="default-src 'none'; font-src ${fontCodicons}; style-src ${webview.cspSource}; script-src 'nonce-${nonce}';">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <link href="${styleResetUri}" rel="stylesheet">
                    <link href="${styleVSCodeUri}" rel="stylesheet">
                    <link href="${styleMainUri}" rel="stylesheet">
                    <link href="${styleCodicons}" rel="stylesheet">

                    <title>[Preview] ${this.fileUri.path.split("/").pop()}</title>
                </head>
                <body>
                    <button id="openOptions" title="Open options">
                        <i class="codicon codicon-gear"></i>
                    </button>
                    <div id="svgContainer"></div>
                    <div id="paramContainer">
                        <div id="buttonContainer">
                            <h3>
                                DOT OPTIONS
                            </h3>
                            <button id="revert" title="Reset selected options">
                                <i class="codicon codicon-discard"></i>
                            </button>
                            <button id="closeOptions" title="Close options">
                                <i class="codicon codicon-remove"></i>
                            </button>
                        </div>
                        <ul id="paramList"></ul>
                    </div>
                </body>
                <script nonce="${nonce}" src="${scriptUri}"></script>
            </html>
            `;
        }
    }
}