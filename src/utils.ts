import * as fs from "fs";

//#region interfaces
export interface ParameterDefinitions {
    parameterDefinitions: Array<ParameterDefinition>,
}
export interface ParameterDefinition {
    id: string,
    name: string,
    description: string,
    category: string,
    type: ParameterType
    isOptional: boolean,
    defaultValue: string,
}

export interface ParameterType {
    valueType: string,
    innerType: Array<ParameterType>,
    possibleValues: Array<string>,
}

export interface ProgressIndication {
    message: string,
    progress: number
}

export interface ResultNotification {
    runToken: string,
    data: string,
	text: string
}
//#endregion


//#region helper functions
export function getNonce() {
    let text = "";
    const possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 32; i++) {
        text += possible.charAt(
            Math.floor(Math.random() * possible.length)
        );
    }
    return text;
}

export function pathExists(p: string): boolean {
    try {
        fs.accessSync(p);
    } catch (err) {
        return false;
    }
    return true;
}
//#endregion
