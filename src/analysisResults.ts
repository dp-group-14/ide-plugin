import * as fs from "fs";
import * as vscode from "vscode";
import * as path from 'path';
import { pathExists } from "./utils";

export class AnalysisResultsProvider
	implements vscode.TreeDataProvider<Result> {

	private _onDidChangeTreeData: vscode.EventEmitter<Result | undefined | null | void> =
		new vscode.EventEmitter<Result | undefined | null | void>();
	readonly onDidChangeTreeData: vscode.Event<Result | undefined | null | void> =
		this._onDidChangeTreeData.event;

	private jsonObject: any;
	private textOutput: string;
	private modestFileExt: string;
	private modestFile: string;
	private treeRoot: Result;

	constructor() {
		this.jsonObject = null;
		this.textOutput = "";
		this.modestFileExt = "";
		this.modestFile = "";
		this.treeRoot = new Result("", "", 0, null);
	}

	refresh(): void {
		this._onDidChangeTreeData.fire();
	}

	getTreeItem(element: Result): vscode.TreeItem {
		return element;
	}

	getChildren(element?: Result): Thenable<Result[]> {
		if (element) {
			var res: Result[] = [];
			if (element.getValues()) {
				res = res.concat(element.getValues());
			}
			if (element.getData()) {
				res = res.concat(element.getData());
			}
			return Promise.resolve(res);
		} else {
			if (this.jsonObject) {
				// this.treeRoot = this.rootToResult(this.jsonObject);
				return Promise.resolve([this.treeRoot]);
			} else {
				return Promise.resolve([]);
			}
		}
	}

	getParent(element: Result) {
		return element.getParent();
	}

	/**
	 * Sets the JSON object of the Tree Provider and refreshes the tree.
	 * @param jsonObject The JSON object that contains the analysis results.
	 * @param textOutput (optional) The text version of the analysis results.
	 */
	setJsonObject(jsonObject: JSON | any, textOutput: string = "") {
		if (jsonObject) {
			this.jsonObject = jsonObject;
			this.textOutput = textOutput;
			this.modestFileExt = jsonObject["file"].split("/").pop();
			this.modestFile = this.modestFileExt.split(".")[0];
			this.treeRoot = this.rootToResult(this.jsonObject);
		} else {
			this.jsonObject = null;
			this.textOutput = "";
			this.modestFileExt = "";
			this.modestFile = "";
			this.treeRoot = new Result("", "", 0, null);
		}
		this.refresh();
	}

	/**
	 * Parses a string to a JSON object and calls the `setJsonObject` function
	 * with that JSON object.
	 * @param {string} jsonString The analysis results in JSON format as a string.
	 */
	setJsonString(jsonString: string) {
		try {
			var jsonObject = JSON.parse(jsonString.trim());
		} catch(err) {
			vscode.window.showErrorMessage("Could not open analysis results.");
		}
		this.setJsonObject(jsonObject);
	}

	/**
	 * Reads a (JSON) file, parses that to a JSON object and calls the
	 * `setJsonObject` function with that JSON object.
	 * @param {string} path The path of the JSON file.
	 */
	setJsonPath(path: string) {
		if (pathExists(path)) {
			var jsonFile = fs.readFileSync(path, "utf-8");
			this.setJsonString(jsonFile);
		}
	}

	/**
	 * Creates the tree root `Result` out of the JSON object.
	 * Structure of the tree view is defined as follows:
	 * - (filename)
	 *   - Tool: (tool name)
	 *   - Open constants: (number of open constants)
	 *     - (open constant name): (open constant value)
	 *     - ...
	 *   - Analysis results
	 *     - (data): (value)
	 *       - (values): (value)
	 *       - (data): (value)
	 *     - ...
	 * @param root The part of the JSON object which should become the root of the tree view.
	 * @returns {Result} The root `Result` of the tree.
	 */
	rootToResult(root: JSON | any): Result {
		var res = new Result(this.modestFileExt, "", vscode.TreeItemCollapsibleState.Expanded, null, true);

		var values: Result[] = [];
		if (root["tool"]["name"]){
			const toolName = new Result(
				"Tool",
				root["tool"]["name"],
				vscode.TreeItemCollapsibleState.None,
				res
			);
			values.push(toolName);
		}
		var data: Result[] = [];
		if (root["open-parameter-values"]){
			const openParameterValues = new Result(
				"Open constants",
				`(${root["open-parameter-values"].length})`,
				vscode.TreeItemCollapsibleState.Collapsed,
				res,
			);
			openParameterValues.setValues(this.valuesToResults(root["open-parameter-values"], res));
			data.push(openParameterValues);
		}

		var analysisResultsData: Result;
		// Tool: Check or simulate
		if (root["data"].length === 1 && !root["data"][0]["group"] && !root["data"][0]["group"]) {
			// Tool: Simulate
			analysisResultsData = new Result(
				"Analysis results",
				"",
				vscode.TreeItemCollapsibleState.Expanded,
				res
			);
			analysisResultsData.setValues(this.valuesToResults(root["data"][0]["values"], analysisResultsData));
			analysisResultsData.setData(this.dataToResult(root["data"][0]["data"], analysisResultsData));
		} else {
			// Tool: Check
			analysisResultsData = new Result(
				"Analysis results",
				"",
				vscode.TreeItemCollapsibleState.Expanded,
				res
			);
			analysisResultsData.setData(this.dataToResult(root["data"], analysisResultsData));
		}
		data.push(analysisResultsData);

		res.setValues(values);
		res.setData(data);

		return res;
	}

	/**
	 * Creates a `Result` array out of a `data` JSON array, that can be
	 * assigned to the parent in `Result.data` of the parent `Result`.
	 * @param {JSON[]} data The `data` JSON array.
	 * @param {Result | null} parent The parent `Result`.
	 * @returns {Result[]} `Result` array of `data` children of a `Result`.
	 */
	dataToResult(data: JSON[], parent: Result | null): Result[] {
		var results: Result[] = [];

		if (data) {
			data.forEach((element: { [x: string]: any }) => {
				var label = "";
				if (element["group"]) {
					label = element["group"];
				} else if (element["property"]) {
					label = element["property"];
				}
				var value = String(element["value"] ?? "");
				var res = new Result(
					label,
					value,
					vscode.TreeItemCollapsibleState.Collapsed,
					parent
				);

				res.setValues(this.valuesToResults(element["values"], res));
				res.setData(this.dataToResult(element["data"], res));

				results.push(
					res
				);
			});
		}

		return results;
	}

	/**
	 * Creates a `Result` array out of a `values` JSON array, that can be
	 * assigned to the parent in `Result.values` of the parent `Result`.
	 * Since a value is a tree leaf, the tree item does not have any `data`
	 * or `values` itself.
	 * @param {JSON[]} values The `values` JSON array.
	 * @param {Result} parent The parent `Result`.
	 * @returns {Result[]} `Result` array of `values` children of a `Result`.
	 */
	valuesToResults(values: JSON[], parent: Result | null): Result[] {
		var results: Result[] = [];

		if (values) {
			values.forEach((v: { [x: string]: any }) => {
				var label = String(v["name"]);
				var unit = v["unit"] ? " " + String(v["unit"]) : "";
				var value = v["value"] + unit;

				results.push(
					new Result(
						label,
						value,
						vscode.TreeItemCollapsibleState.None,
						parent
					)
				);
			});
		}

		return results;
	}

	getJsonObject() {
		return this.jsonObject;
	}

	getModestFile() {
		return this.modestFile;
	}

	getTreeRoot() {
		return this.treeRoot;
	}

	getTextOutput() {
		return this.textOutput;
	}
}

/**
 * A `Result` is a single tree item in an analysis results provider.
 */
export class Result extends vscode.TreeItem {
	private values: Result[];
	private data: Result[];

	constructor(
		public readonly label: string,
		private value: string,
		public readonly collapsibleState: vscode.TreeItemCollapsibleState,
		private parent: Result | null,
		private isRoot: boolean = false,
		private isPlottable: boolean = false
	) {
		super(label, collapsibleState);
		if (value !== "") {
			this.tooltip = `${this.label}: ${this.value}`;
		} else {
			this.tooltip = this.label;
		}
		this.values = [];
		this.data = [];
		this.description = this.value;
		this.parent = parent;
		this.isRoot = isRoot;

		if (isRoot) {
			if (label.endsWith(".modest")) {
				this.iconPath = {
					light: path.join(__filename, '..', '..', 'media', 'modest-icon.svg'),
					dark: path.join(__filename, '..', '..', 'media', 'modest-icon.svg')
				};
			}
			// Icons for other types of model formats (e.g. JANI) can be added here...
		}

		const plottableRegex = /\((-?\d+\.?\d*[eE]?-?\d*),\s*(-?\d+\.?\d*[eE]?-?\d*)\)/gm; // (num, num), num in the form of -1, 2.3, 4e5, 4.5e6
		if (value.match(plottableRegex)) {
			this.isPlottable = true;
		}

		var cVal = "";
		cVal += this.value === "" ? "empty, " : "notEmpty, ";
		cVal += this.isRoot ? "root, " : "notRoot, ";
		cVal += this.isPlottable ? "plot, " : "";
		this.contextValue = cVal;
	}

	getParentName() {
		return this.parent?.getLabel();
	}

	getParent() {
		return this.parent;
	}

	getLabel() {
		return this.label;
	}

	getValue() {
		return this.value;
	}

	getValues() {
		return this.values;
	}
	setValues(values: Result[]) {
		this.values = values;
	}

	getData() {
		return this.data;
	}
	setData(data: Result[]) {
		this.data = data;
	}

	getIsPlottable() {
		return this.isPlottable;
	}

	/**
	 * Highlights a `Result` by putting an icon in front of it.
	 */
	highlight() {
		if (this.iconPath !== undefined) {
			this.iconPath = undefined;
		} else {
			this.iconPath = {
				light: path.join(__filename, '..', '..', 'media', 'light', 'circle.svg'),
				dark: path.join(__filename, '..', '..', 'media', 'dark', 'circle.svg')
			};
		}
	}
}
