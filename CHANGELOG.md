# Change Log

All notable changes to the "modestextension" extension will be documented in this file.

## [0.0.3]
- Made plot view scale with its size
- Added a snippet for functions
- Opening and saving the JSON analysis results will prettify the JSON first
- Updated internal names of commands

## [0.0.2]
*Note this update requires your version of the Modest Toolset to be updated as well*
- Experimental support for running tools on JANI files (make sure the file has the .jani extension)
- Tool problems (e.g. skipping a property) can be cleared using quickfixes or by clicking the button in the side bar
- Bug fixes

## [0.0.1]
- Initial release