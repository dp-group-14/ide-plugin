

const vscode = acquireVsCodeApi();
const svgContainer = document.getElementById("svgContainer");
const paramContainer = document.getElementById("paramContainer");
const paramList = document.getElementById("paramList");

var svgImage;

var parameterDefinitions;
var viewBox;
var lastPoint;
var newPoint;
var scale;
var optionsOpen = false;


window.addEventListener('message', event => {
    const type = event.data.type;
    switch (type) {
        case 'reset':
            // reset the svg's position
            if (svgImage !== undefined) {
                scale = 1;
                viewBox = { x: 0, y: 0, w: svgImage.clientWidth, h: svgImage.clientHeight };
                svgImage.setAttribute('viewBox', `0 0 ${svgImage.clientWidth} ${svgImage.clientHeight}`);
            }
            break;
        case 'zoomIn':
            zoom(true);
            break;
        case 'zoomOut':
            zoom(false);
            break;
        case 'svg':
            // Handle a newly received svg
            const newSvg = event.data.svg;
            if (svgImage === undefined) {
                svgContainer.innerHTML = newSvg.replace('<svg', '<svg id="svgImage"');
                svgImage = document.getElementById("svgImage");
                scale = 1;
                viewBox = { x: 0, y: 0, w: svgImage.clientWidth, h: svgImage.clientHeight };
                svgImage.setAttribute('viewBox', `0 0 ${svgImage.clientWidth} ${svgImage.clientHeight}`);
            } else {
                // If we already have an svg element, replace the contents
                svgImage.innerHTML = newSvg.match(/(?<=<svg(?:.|\n|\r)+?>)(?:(?:.|\n|\r)+)(?=<\/svg>)/)[0];
            }
            break;
        case 'params':
            // Handle received parameters
            if (paramList.innerHTML === '') {
                event.data.params.forEach(addParameterItem);
                parameterDefinitions = event.data.params;
            }
            break;
        case 'paramValues':
            // Handle received parameter values
            resetParams();
            // Set all the new values
            const values = event.data.values;
            for (const entry of values) {
                const id = entry[0];
                const value = entry[1];
                const valueElem = document.getElementById('value-' + id);
                if (valueElem.type === 'checkbox') {
                    valueElem.checked = value;
                } else {
                    valueElem.value = value;
                }
            }
            break;
        case 'optionsOpen':
            // Toggle the open options
            optionsOpen = event.data.optionsOpen;
            if (event.data.optionsOpen) {
                paramContainer.style.maxHeight = '450px';
            } else {
                paramContainer.style.maxHeight = '0px';
            }
            break;
    }
});


/**
 * resets all parameters in the dot options to their defaults
 */
function resetParams() {
    if (parameterDefinitions) {
        parameterDefinitions.forEach(param => {
            const valueElem = document.getElementById("value-" + param.id);
            switch (param.type.valueType) {
                case 'String':
                    valueElem.value = param.defaultValue;
                    break;
                case 'Boolean':
                    valueElem.checked = param.defaultValue;
                    break;
                case 'Enum':
                    valueElem.value = param.defaultValue;
                    break;
                default:
                    // Shouldn't happen
                    console.log("OKAY I DON'T KNOW THIS TYPE???  " + param.type.valueType);
                    break;
            }
        });
    }
}

/**
 * adds a parameter to the dotOptions window
 * @param {*} param the parameter to be added
 */
function addParameterItem(param) {

    const li = document.createElement("li");

    const nameBox = document.createElement("label");
    nameBox.id = "name-" + param.id;
    nameBox.appendChild(document.createTextNode(param.name));
    nameBox.classList.add("name");
    nameBox.title = param.description;

    li.appendChild(nameBox);

    let valueBox;

    switch (param.type.valueType) {
        case 'String':
            valueBox = document.createElement("input");
            valueBox.id = "value-" + param.id;
            valueBox.value = param.defaultValue;
            valueBox.type = 'text';
            valueBox.addEventListener("input", _ => {
                vscode.postMessage({
                    type: 'paramUpdate',
                    id: param.id,
                    value: valueBox.value
                });
            });
            break;
        case 'Boolean':
            valueBox = document.createElement("input");
            valueBox.id = "value-" + param.id;
            valueBox.checked = param.defaultValue;
            valueBox.type = 'checkbox';
            nameBox.style.cursor = 'pointer';
            nameBox.onclick = () => {
                valueBox.checked = !valueBox.checked;
                vscode.postMessage({
                    type: 'paramUpdate',
                    id: param.id,
                    value: valueBox.checked
                });
            };
            valueBox.addEventListener("input", _ => {
                vscode.postMessage({
                    type: 'paramUpdate',
                    id: param.id,
                    value: valueBox.checked
                });
            });
            break;
        case 'Enum':
            valueBox = document.createElement("select");
            valueBox.id = "value-" + param.id;
            valueBox.classList.add("value");
            for (const value of param.type.possibleValues) {
                const option = document.createElement("option");
                option.value = value;
                option.text = value;
                valueBox.appendChild(option);
            }

            valueBox.value = param.defaultValue;

            valueBox.addEventListener("input", _ => {
                vscode.postMessage({
                    type: 'paramUpdate',
                    id: param.id,
                    value: valueBox.value
                });
            });
            break;
        default:
            // Shouldn't happen
            console.log("OKAY I DON'T KNOW THIS TYPE???  " + param.type.valueType);
            break;
    };

    li.appendChild(valueBox);

    paramList.appendChild(li);

}

// -------------------------- Onclick handlers --------------------------

document.getElementById("openOptions").onclick = () => {
    paramContainer.style.maxHeight = `${Math.min(window.innerHeight - 20, 450)}px`;
    paramList.style.maxHeight = `${Math.min(window.innerHeight - 60, 410)}px`;
    optionsOpen = !optionsOpen;
    vscode.postMessage({
        type: 'toggleOptions',
    });
};

document.getElementById("closeOptions").onclick = () => {
    paramContainer.style.maxHeight = '0px';
    optionsOpen = !optionsOpen;
    vscode.postMessage({
        type: 'toggleOptions',
    });
};

document.getElementById("revert").onclick = () => {
    resetParams();

    vscode.postMessage({
        type: 'paramReset',
    });
};




// Below is code to handle the calculation of the position of the svg. I have no idea how it works exactly.
// FULL CREDIT TO https://stackoverflow.com/questions/52576376/how-to-zoom-in-on-a-complex-svg-structure
// also https://stackoverflow.com/questions/1685326/responding-to-the-onmousemove-event-outside-of-the-browser-window-in-ie
window.addEventListener('resize', () => {
    if (svgImage !== undefined) {
        viewBox.w = svgImage.clientWidth / scale;
        viewBox.h = svgImage.clientHeight / scale;
        svgImage.setAttribute('viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.w} ${viewBox.h}`);
    }
    if (optionsOpen) {
        paramContainer.style.maxHeight = `${Math.min(window.innerHeight - 20, 450)}px`;
        paramList.style.maxHeight = `${Math.min(window.innerHeight - 60, 410)}px`;
    }
});

function zoom(zoomIn) {
    if (svgImage !== undefined) {
        var w = viewBox.w;
        var h = viewBox.h;
        var mx = svgImage.clientWidth / 2;//middle of screen
        var my = svgImage.clientHeight / 2;
        var dw = w * 0.15 * (zoomIn ? 1 : -1);
        var dh = h * 0.15 * (zoomIn ? 1 : -1);
        var svgSize = { w: svgImage.clientWidth, h: svgImage.clientHeight };
        var dx = dw * mx / svgSize.w;
        var dy = dh * my / svgSize.h;
        viewBox = { x: viewBox.x + dx, y: viewBox.y + dy, w: viewBox.w - dw, h: viewBox.h - dh };
        scale = svgSize.w / viewBox.w;
        svgImage.setAttribute('viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.w} ${viewBox.h}`);
    }
}

svgContainer.onmousewheel = (e) => {
    if (svgImage !== undefined) {
        var w = viewBox.w;
        var h = viewBox.h;
        var mx = e.offsetX; //mouse x
        var my = e.offsetY;
        var dw = w * Math.sign(-e.deltaY) * 0.15;
        var dh = h * Math.sign(-e.deltaY) * 0.15;
        var svgSize = { w: svgImage.clientWidth, h: svgImage.clientHeight };
        var dx = dw * mx / svgSize.w;
        var dy = dh * my / svgSize.h;
        viewBox = { x: viewBox.x + dx, y: viewBox.y + dy, w: viewBox.w - dw, h: viewBox.h - dh };
        scale = svgSize.w / viewBox.w;
        svgImage.setAttribute('viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.w} ${viewBox.h}`);
    }
};

svgContainer.onmousedown = (e) => {
    if (svgImage !== undefined && e.button !== 1) {
        lastPoint = { x: e.x, y: e.y };

        document.onmousemove = (e) => {
            newPoint = { x: e.x, y: e.y };
            viewBox.x += (lastPoint.x - newPoint.x) / scale;
            viewBox.y += (lastPoint.y - newPoint.y) / scale;
            svgImage.setAttribute('viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.w} ${viewBox.h}`);
            lastPoint = newPoint;
        };

        document.onmouseup = () => {
            document.onmousemove = null;
        };
    }
};



